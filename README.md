# chde-wallpaper
Wallpaper repo for CHDE.

# Installation
To get these wallpapers, clone this repo, or install the chde-wallpaper package from the CHDE repo.

# Credits
I'm not entirely sure where these wallpapers are from, but I do know they come with ArcoLinux. If these wallpapers belongs to you, let me know and I will take down/credit you.
